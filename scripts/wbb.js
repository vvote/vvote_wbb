/**
 *   This file is part of vVote from the Victorian Electoral Commission.
 *
 *   vVote is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License.
 *
 *   vVote is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with vVote.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Contact Craig Burton   craig.burton@vec.vic.gov.au
 *
 *
 * ScanVisualScreen - The visual screen class for the scan qr code screen for the visual interface.
 * 
 * @author Peter Scheffer
 */

if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

var BaseServiceURL = "../WbbServices";

var URL = {
    BulkQuarantineStaticURL: BaseServiceURL + "/data/Quarantine.zip",
    VotesStaticURL: BaseServiceURL + "/data/votes.zip",
    CommitsStaticURL: BaseServiceURL + "/commits/all.zip",
    AudioFileURL: BaseServiceURL,

    ReceiptServiceURL: BaseServiceURL + "/ReceiptService.svc/api/GetReceipt",
    AudioServiceURL: BaseServiceURL + "/AudioService.svc/api/GetCandidateAudioList",
}

var findReceiptObj = {
    
    validateSerialNo: function (receiptNumber, successCallback, errorCallback) {
        if (/^[0-9]+:[0-9]+$/.test(receiptNumber)) {
            successCallback(receiptNumber);
        } else {
            errorCallback("You have entered an invalid receipt number.<br />The format of the receipt is 1234567:89");
        }
    },

    getReceipt: function (receiptNumber, successCallback, errorCallback) {
        var query = JSON.stringify({ "SerialNumber": receiptNumber });

        $.ajax({
            type: 'POST',
            url: URL.ReceiptServiceURL,
            data: query,
            dataType: 'json',
            contentType: 'application/json',
            success: successCallback,
            error: errorCallback
        });
    }
};

var commonObj = {
    getQueryStringParameter: function (key) {
        return decodeURIComponent((new RegExp('[?|&]' + key + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null
    },

    checkFileExist: function (url, existCallback, notExistCallback) {
        $.ajax({
            url: url,
            type: 'HEAD',
            success: function () {
                existCallback();
            },
            error: function () {
                notExistCallback();
            }
        });
    }
};