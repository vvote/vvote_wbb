vVote_WBB
==========

Public website which provides verification evidence, system information and configuration evidence
--------------------------------------------------------------------------------------------------
 
This is part of the vVote design.  It has been implemented by the Victorian Electoral Commission.
It consists of HTML / CSS and Javascript content for a common web server (IIS) as well as a .NET server to service receipt-lookup requests from the public.

A receipt is given to all vVote users when they finish voting.  It captures the entire vote, but in the Pret a voter format to protect privacy and secrecy.
 
The WBB website provides:
 
  * Inhertiting frameset (currently from vec.vic.gov.au)
  * Tab 1: home tab - information about the system
  * Tab 2: lookup my receipt - key a serial number from vVote and the system will retrieve one of candidate list audit; voted preference list (receipt); candidate list quarantine.
  * Tab 3: candidate audio list - all synthetic placeholders or final human agent recorded candidate names as MP3 links
  * Tab 4: verification - provided after close of elections.  This provides: raw vote files (CSV) and proof files (decryption, shuffle, packing, ballot generation) and executable tools to perform proof checks on the files.

vVote_WBB has a mirror called WBBM which is the first point of contact for the SuVote Private Web Bullentin Board (MBB) system to post signed, agreed submissions (receipts, audits, data) forming the daily commits the system makes.
The vVote client configuration is also provided on WBBM.
WBBM content is mirrored to WBB.
